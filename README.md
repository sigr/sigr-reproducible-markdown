# Ecole thématique Sciences de l'information géographique reproductibles SIGR

[Ecole thématique CNRS](https://sigr2020.sciencesconf.org/resource/page/id/2), 27 juin au 2 juillet 2021 à Saint-Pierre d'Oléron. 

## Atelier Outils de la recherche reproductible

Ce présentation vise à présenter les enjeux du litterate programing pour documenter et partager efficacement ses chaines de traitement. Elle est réalisée pour l'atelier **Outils de la recherche reproductible**, animé par Timothée Giraud, Ronan Ysebaert et Nicolas Lambert (UMS RIATE).

Elle est accessible [ici](https://sigr.gitpages.huma-num.fr/sigr-reproducible-markdown)




